#+title: Site Notes
#+date: 2021-06-02
#+draft: false
#+categories[]: site
#+tags[]: todo site

I will keep notes to this blog in this file using org mode.

** Notes Org Mode
   [[https://www.orgmode.org/][Org mode]] is supported by default in [[https://gohugo.io/][hugo]]. 
  
** Todo list for this site

*** DONE Use at least default [[https://gohugo.io/content-management/taxonomies/#default-taxonomies][taxonomies]]
*** DONE Implement rss sindication
*** DONE Implement search following [[https://gist.github.com/eddiewebb/735feb48f50f0ddd65ae5606a1cb41ae][fuse.js integration]]
*** TODO Setup date last modification for posts
*** TODO Watch another [[https://www.youtube.com/watch?v=dljNabciEGg][video]] 
*** TODO Research if can I mix posts both in english and spanish
