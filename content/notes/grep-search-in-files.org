#+title: Search for a string in a given folder recursively
#+date: 2021-07-01
#+draft: false
#+categories[]: code
#+tags[]: bash scripts

If you are looking for a text pattern to match
in several files at a time you can use the fabulous 
grep command.

#+BEGIN_SRC bash
  $ grep -r 'text to search for' ./
#+END_SRC
