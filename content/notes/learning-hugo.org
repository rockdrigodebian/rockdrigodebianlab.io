#+title: Learning Hugo
#+date: 2021-06-02
#+draft: false
#+categories[]: show
#+tags[]: web publish free

I am learning [[https://gohugo.io/][hugo]]. As of today I have published 3 sites using it including this one. The thing I like the most about hugo 
is that you can re-use knowledge you already have to deploy a static web site to the Internet for free. 
In order to deploy those sites I have used github and gitlab pages.

- [[https://www.confeccionescolombia.com/][an e-comerce]] [[https://github.com/rogithub/ccsite][repo]]
- [[https://rockdrigoj.github.io/][site for unit tests]] [[https://github.com/rogithub/webtesting][repo]]
- [[https://rockdrigodebian.gitlab.io/][this blog]] [[https://gitlab.com/rockdrigodebian/rockdrigodebian.gitlab.io][repo]]

Both are pretty easy to use, to my experience using gitlab is the easiest if you are hosting your site and 
deploying it to the same repository. However on github, I was able to do things like having code in one 
repo and deploying site to another account's repo.


* Why I moved from markdown to org mode
Markdown is a pretty convenient format to add content to your site, however there was one thing that make me switch
to org mode, it was tables. Table editing is much less of a pain in org mode. In general I think org is more cappable
than markdown.
