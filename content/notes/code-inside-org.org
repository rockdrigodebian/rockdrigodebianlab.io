#+title: Insert code in org mode
#+date: 2021-06-03
#+draft: false
#+categories[]: code
#+tags[]: org-mode org

To insert source code to org files

1. Open emacs
2. Write: <s 
3. Then press tab